#include "Arduino.h"
#include "ringbuffer.h"

// =============================================================== // Constants
const char txBufferSize = 5;
const char END_OF_FRAME = '\r';
const char LIVE_MODE = 'L';
const char READ = 'R';
const char WRITE = 'W';
const char TEMPERATURE = 'T';
const char VOLTAGE = 'V';
const char THROTTLE = 'H';
const char MOTOR_SPEED = 'M';
const char ACTUAL_SPEED = 'A';
const char DUTY_CYCLE = 'D';
const char MOTOR_CURRENT = 'C';
const char WHEEL_DIAMETER = 'd';
const char WHEEL_GEAR_TEETH = 'w';
const char MOTOR_GEAR_TEETH = 'm';
const char MOTOR_KV_VALUE = 'k';

const long liveModeInterval = 1000;

// =============================================================== // Variables
ring_buffer_t rxBuffer;
char txBuffer[txBufferSize];

bool isInLiveMode = false;
unsigned long previousMillis = 0;
unsigned long currentMillis = 0;

// =============================================================== // Board Parameters
struct Parameter {
  char Id;
  uint16_t Value;
};

Parameter parameters[11] = {
  {TEMPERATURE, 35},
  {VOLTAGE, 42},
  {THROTTLE, 60},
  {MOTOR_SPEED, 315},
  {ACTUAL_SPEED, 24},
  {DUTY_CYCLE, 64},
  {MOTOR_CURRENT, 2065},
  {WHEEL_DIAMETER, 800},
  {WHEEL_GEAR_TEETH, 15},
  {MOTOR_GEAR_TEETH, 34},
  {MOTOR_KV_VALUE, 120},
};

Parameter* getParameterById(char id) {
  for (size_t i = 0; i < 11; i++) {
    if (parameters[i].Id == id) {
      return &parameters[i];
    }
  }
}

bool isConfigurable(char parameterId) {
  switch (parameterId) {
    case WHEEL_DIAMETER:
    case WHEEL_GEAR_TEETH:
    case MOTOR_GEAR_TEETH:
    case MOTOR_KV_VALUE:
      return true;
    default:
      return false;
  }
}

struct Frame {
  char identifier;
  char readOrWrite;
  uint16_t data;
};


// =============================================================== // Functions

void updateParameter(char parameterId, uint16_t value) {
  Parameter *parameter = getParameterById(parameterId);
  (*parameter).Value = value;
  Serial.println("Update param " + String((*parameter).Id) + " With value " + String((*parameter).Value) + " <= " + String(value));
}

// Send some mock data to mobile app
void sendParameterValue(char parameterId) {
  uint16_t data = 0;
  Parameter parameter = *getParameterById(parameterId);

  if (isConfigurable(parameterId)) {
    data = parameter.Value;
  } else {
    int noise = 0.2 * parameter.Value;
    data = parameter.Value + random(-noise, noise);
  }

  uint8_t hi = ((data >> 8) & 0xff);
  uint8_t lo = ((data >> 0) & 0xff);

  txBuffer[0] = parameterId;
  txBuffer[1] = WRITE;
  txBuffer[2] = hi;
  txBuffer[3] = lo;
  txBuffer[4] = END_OF_FRAME;

  // Serial.write(txBuffer, txBufferSize);
  Serial1.write(txBuffer, txBufferSize);
}

// Get last four items from the buffer
Frame getFrameFromBuffer() {
  Frame frame;
  char dataByteHi;
  char dataByteLo;

  uint8_t top = ring_buffer_num_items(&rxBuffer);

  ring_buffer_peek(&rxBuffer, &frame.identifier, top - 4);
  ring_buffer_peek(&rxBuffer, &frame.readOrWrite, top - 3);
  ring_buffer_peek(&rxBuffer, &dataByteHi, top - 2);
  ring_buffer_peek(&rxBuffer, &dataByteLo, top - 1);

  // Serial.write(frame.identifier);
  // Serial.write(frame.readOrWrite);
  // Serial.write(dataByteHi);
  // Serial.write(dataByteLo);
  // Serial.write(END_OF_FRAME);

  frame.data = (dataByteHi << 8) | (dataByteLo & 0xFF);

  return frame;
}

// Called when END_OF_FRAME byte is received
void frameReceived() {
  Frame frame = getFrameFromBuffer();

  // Serial.print("Frame received: ");
  // Serial.println(frame.identifier);

  switch(frame.identifier) {
    case LIVE_MODE:
      if (frame.data == 1)
        isInLiveMode = true;
      else
        isInLiveMode = false;
      break;
    case TEMPERATURE:
    case VOLTAGE:
    case THROTTLE:
    case MOTOR_SPEED:
    case ACTUAL_SPEED:
    case DUTY_CYCLE:
    case MOTOR_CURRENT:
    case WHEEL_DIAMETER:
    case WHEEL_GEAR_TEETH:
    case MOTOR_GEAR_TEETH:
    case MOTOR_KV_VALUE:
      if (frame.readOrWrite == READ)
        sendParameterValue(frame.identifier);
      else
        updateParameter(frame.identifier, frame.data);
      break;
    default:
      Serial.print("Unknown identifier in frame:");
      Serial.println(frame.identifier);
  }
}

// =============================================================== // Setup
void setup() {
  Serial.begin(9600);  // USB Serial (for debugging)
  Serial1.begin(9600); // BLE module
  ring_buffer_init(&rxBuffer);
}

// =============================================================== // Loop
void loop() {
  char byte = 0x00;

  if (Serial.available() || Serial1.available()) {
    if (Serial.available()) byte = Serial.read();
    if (Serial1.available()) byte = Serial1.read();

    if (byte == END_OF_FRAME) {
      frameReceived();
    } else {
      ring_buffer_queue(&rxBuffer, byte);
    }
  }

  currentMillis = millis();
  if (currentMillis - previousMillis >= liveModeInterval) {
    previousMillis = currentMillis;

    if (isInLiveMode) {
      sendParameterValue(VOLTAGE);
      delay(100);
      sendParameterValue(TEMPERATURE);
    }
  }
}
